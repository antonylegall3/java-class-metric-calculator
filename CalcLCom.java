import java.io.*;
/**
 * Calculate the Lack of Cohesion in Methods 
 * for a given parsed class file
 *
 * @author Antony LeGall
 */
public class CalcLCom
{
	public static int calcLCom(ClassFile cf) throws CodeParsingException, InvalidConstantPoolIndex
	{
		Method[] methods = cf.getMethods();
		ConstantPool constantPool = cf.getConstantPool();
		Field[] fields = cf.getFields();
		CPEntry cpEntry;
		Method method;
		Attribute attribute = null;
		byte[] bytes;
		Instruction instruction;
		int index, index1, index2, offset = 0;
		ConstantRef ref;
		int num = getSetSize(fields, methods);
		DisjointUnionSets disjointSets = new DisjointUnionSets(num);
		String[] strArray = createStringArray(fields, methods, num);

		//Read all the Code attributes from every method
		for(int i = 0; i < methods.length; i++)
		{
			method = methods[i];
			//Skip null methods such as <init>
			if(method != null)
			{
				//Get the Attributes from each method
				attribute = method.getAttribute();

				//Get the byte array for each methods code attribute
				bytes = attribute.getCodeArray();
				offset = 0;
				//Parse the byte array one instruction at a time
				while(offset < bytes.length)
				{
					index = 0;

					//Create instructions and choose the ones that pertain to method/field refs
					instruction = createInstruction(bytes, offset);
					if(instruction.getValidRef() == true)
					{
						//Resolve the index into the constant pool
						index = (int)(((bytes[instruction.getOffset() + 1] & 0xFF) << 8) |
							(bytes[instruction.getOffset() + 2] & 0xFF));
						cpEntry = constantPool.getEntry(index);
						//Check the type of reference to dictate how you handle the indexing 
						if(cpEntry instanceof ConstantMethodRef)
						{
							ref = (ConstantRef)cpEntry;
							if(!ref.getName().equals("equals"))
							{
								//Get the indexes of the strings that correspond to their indexes in the disjoint set
								index1 = findEntry(strArray, ref.getName());
								index2 = findEntry(strArray, method.getName() + method.getDescriptor());
								//-1 Indicates a string not present in our disjoint set
								if((index1 != -1) && (index2 != -1))
								{
									disjointSets.union(index1, index2);	
								}
							}
						}
						else if(cpEntry instanceof ConstantFieldRef)
						{
							ref = (ConstantRef)cpEntry;
							//Get the indexes of the strings that correspond to their indexes in the disjoint set
							index1 = findEntry(strArray, ref.getName());
							index2 = findEntry(strArray, method.getName());
							//-1 Indicates a string not present in our disjoint set
							if((index1 != -1) && (index2 != -1))
							{
								disjointSets.union(index1, index2);	
							}
						}
					}
					offset += instruction.getSize();
				}
			}
		}
		return disjointSets.getNumSets();
	}

	public static Instruction createInstruction(byte[] bytes, int offset) throws CodeParsingException
	{
		Instruction instruction = new Instruction(bytes, offset);
		Opcode opCode = instruction.getOpcode();
		switch(opCode)
		{
			case GETSTATIC: case GETFIELD: case PUTFIELD: case INVOKEVIRTUAL:
			case INVOKESPECIAL: case INVOKESTATIC: case INVOKEINTERFACE:
			case NEW:
				//Indicate that this Instruction references a method/field
				instruction.setValidRef(true);
				break;
		}
		return instruction;
	}

	public static int getSetSize(Field[] fields, Method[] methods)
	{
		//Count the number of fields non final and methods not init
		int num = 0;
		//Count the number of important fields and methods from our class file
		//Important meaning not constant/ not a static initializer
		for(int x = 0; x < fields.length; x++)
		{
			if(!fields[x].getAccessFlags().contains("final"))
			{
				num++;
			}
		}
		for(int y = 0; y < methods.length; y++)
		{
			if(methods[y] != null)
			{
				num++;
			}
		}
		return num;
	}

	public static String[] createStringArray(Field[] fields, Method[] methods, int size)
	{
		int num = 0;
		String[] array = new String[size];
		//Insert the names of all important fields and methods into an array of strings
		// to be used to correspond with the disjoint sets
		for(int x = 0; x < fields.length; x++)
		{
			if(!fields[x].getAccessFlags().contains("final"))
			{
				array[num] = fields[x].getName();
				num++;
			}
		}
		for(int y = 0; y < methods.length; y++)
		{
			if(methods[y] != null)
			{
				array[num] = methods[y].getName();
				num++;
			}
		}
		return array;
	}

	public static int findEntry(String[] strArray, String name)
	{
		int index = -1;
		//Find the index at which the provided string occurs in the array
		for(int i = 0; i < strArray.length; i++)
		{
			if(strArray[i].equals(name))
			{
				index = i;
			}
		}
		return index;
	}

}