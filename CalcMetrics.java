import java.io.*;

/**
 * Parses and displays a Java .class file.
 *
  * #NOTE: Previously ClassFileParser
 * @author David Cooper
 * Modified by Antony LeGall
 */
public class CalcMetrics
{
    public static void main(String[] args)
    {
        if(args.length > 0)
        {
            System.out.println("LCOM4:");
            ClassFile[] classArray = new ClassFile[args.length];
            //Iterate once for each command line parameter
             try
             {
                for(int i = 0; i < args.length; i++)
                {
                        String str = "";
                        ClassFile cf = new ClassFile(args[i]);
                        str = String.format(
                        "   %s:%d\n",cf,CalcLCom.calcLCom(cf));
                        System.out.print(str);
                        classArray[i] = cf;
                    }

                    int cBo = CalcCBO.calcCBO(classArray);
                    double result = (double)cBo/args.length;
                    String str;
                    str = String.format(
                        "\nCBO: %d/%d = %.1f\n", cBo, args.length, result);
                    System.out.print(str);
                }
                catch(IOException e)
                {
                    System.out.printf("Cannot read \"%s\": %s\n",
                        args[0], e.getMessage());
                }
                catch(ClassFileParserException e)
                {
                    System.out.printf("Class file format error in \"%s\": %s\n",
                        args[0], e.getMessage());
                }
        }
        else
        {
            System.out.println("Usage: java ClassFileParser <class-file>");
        }
    }
}
