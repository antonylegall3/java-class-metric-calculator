import java.io.*;

/**
 * Parse and return an attribute
 *
 * @author Antony LeGall
*/

public abstract class Attribute
{
	public static Attribute parseAttribute(DataInputStream dis, String attributeName)  throws IOException
	{
		Attribute attribute = null;
		switch(attributeName)
		{
			case "Code":
				attribute = new CodeAttribute(dis);
				break;
		}
		return attribute;
	}

	public abstract byte[] getCodeArray();

	public abstract String getName();

}

/**
 * Class Extending Attribute to provide fields to read in and store
 * all information pertaining to code attributes
 */

class CodeAttribute extends Attribute
{
	private String name;
	private int length;
	private int maxStack;
	private int maxLocals;
	private int codeLength;
	private byte[] codeArray;
	private int exceptionTableLength;
	private ExceptionEntry[] exceptionTable;

	public CodeAttribute(DataInputStream dis) throws IOException
	{
		//Parse the entire code attribute storing it in fields
		name = "Code";
		length = dis.readInt();
		maxStack = dis.readUnsignedShort();
		maxLocals = dis.readUnsignedShort();

		codeLength = dis.readInt();
		codeArray = new byte[codeLength];
		for(int i = 0; i < codeLength; i++)
		{
			codeArray[i] = dis.readByte();
		}

		exceptionTableLength = dis.readUnsignedShort();
		exceptionTable = new ExceptionEntry[exceptionTableLength];
		for(int j = 0; j < exceptionTableLength; j++)
		{
			exceptionTable[j] = new ExceptionEntry(dis.readUnsignedShort(), dis.readUnsignedShort(),
				dis.readUnsignedShort(), dis.readUnsignedShort());
		}

		//Skip the attributes attributes as they are not used
		int count = dis.readUnsignedShort();
		for(int l = 0; l < count; l++)
		{
			dis.readUnsignedShort();
			int temp = dis.readInt();
			for(int k = 0; k < temp; k++)
			{
				dis.readByte();
			}	
		}
	}

	public String getName()
	{
		return name;
	}

	public int getLength() { return length;}

	public byte[] getCodeArray() { return codeArray;}
}

/**
 * Class used to provide fields to store each entry in the exception table
 */

class ExceptionEntry
{
	private int startpc;
	private int endpc;
	private int handlerpc;
	private int catchType;

	public ExceptionEntry(int inStartPc, int inEndPc, int inHandlerPc, int inCatchType)
	{
		startpc = inStartPc;
		endpc = inEndPc;
		handlerpc = inHandlerPc;
		catchType = inCatchType;
	}
}