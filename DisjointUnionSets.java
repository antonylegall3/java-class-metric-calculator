// A Java program to implement Disjoint Set Data
// Structure.
import java.io.*;
import java.util.*;
 
class DisjointUnionSets
{
    int[] rank, parent;
    int n;
    int numSets;
 
    // Constructor
    public DisjointUnionSets(int n)
    {
        rank = new int[n];
        parent = new int[n];
        numSets = n;
        this.n = n;
        makeSet();
    }
 
    // Creates n sets with single item in each
    void makeSet()
    {
        for (int i=0; i<n; i++)
        {
            // Initially, all elements are in
            // their own set.
            parent[i] = i;
        }
    }
 
    // Returns representative of x's set
    int find(int x)
    {
        // Finds the representative of the set
        // that x is an element of
        if (parent[x]!=x)
        {
            // if x is not the parent of itself
            // Then x is not the representative of
            // his set,
            parent[x] = find(parent[x]);
 
            // so we recursively call Find on its parent
            // and move i's node directly under the
            // representative of this set
        }
        return parent[x];
    }


    // Unites the set that includes x and set y
    void union(int x, int y)
    {
        // Find representatives of two sets
        int xRoot = find(x), yRoot = find(y);
 
        // Elements are in the same set, no need join
        if (xRoot == yRoot)
        {
            return;
        }
        else
        {
            //Join the sets and decrement the numSets
            parent[yRoot] = xRoot;
            numSets--;
        }
    }

    public int getSize()
    {
        return n;
    }

    public int getNumSets()
    {
        return numSets;
    }
}