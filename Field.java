import java.io.*;

/**
 * Parse the class fields belonging to a class and resolve their access
 * flags, names and datatypes.
 *
 * @author Antony LeGall
 */
public class Field
{
	private String accessFlags;
	private String name;
	private String dataType;

	public Field(String inAccessFlags, String inName, String inDataType)
	{
		accessFlags = inAccessFlags;
		name = inName;
		dataType = inDataType;
	}

	public String getAccessFlags()
	{
		return accessFlags;
	}

	public String getName()
	{
		return name;
	}

	public String getDataType()
	{
		return dataType;
	}
	
	public static Field[] parseFields(int count, ConstantPool constantPool,
			DataInputStream dis) throws ClassFileParserException,
                                             IOException
	{
		Field[] fields = new Field[count];
		String flags, fieldName = null, dataType = null;
		CPEntry cp;
		ConstantUtf8 c8;
		int entry, attributeCount, attributeSize;
		for(int i = 0; i < count; i++)
		{
			//Get the Access Flags
			flags = parseAccessFlags(dis.readUnsignedShort());

			//Get the Field Name
			entry = dis.readUnsignedShort();
			cp = constantPool.getEntry(entry);
			c8 = (ConstantUtf8) cp;
			fieldName = c8.getBytes();
			
			//Get the Data Type
			entry = dis.readUnsignedShort();
			cp = constantPool.getEntry(entry);
			c8 = (ConstantUtf8) cp;
			dataType = resolveDataType(c8.getBytes());

			//Get the Count
			attributeCount = dis.readUnsignedShort();
			//Fields will never have code attributes so always discard their attributes
			if(attributeCount != 0)
			{
				for(int j = 0; j < attributeCount; j++)
				{
					//Read the name then the length then skip ahead length times
					dis.readUnsignedShort();
					attributeSize = dis.readInt();
					for(int k = 0; k < attributeSize; k++)
					{
						dis.readByte();
					}
				}
			}
			fields[i] = new Field(flags, fieldName, dataType);
		}
		return fields;
	}

	public static String parseAccessFlags(int accessFlag)
	{
		//Performs an AND operation with each access flag to determine
		// flags are present
		final int ACC_PUBLIC = 0x0001;
		final int ACC_PRIVATE = 0x0002;
		final int ACC_PROTECTED = 0x0004;
		final int ACC_STATIC = 0x0008;
		final int ACC_FINAL = 0x0010;
		final int ACC_VOLATILE = 0x0040;
		final int ACC_TRANSIENT = 0x0080;
		final int ACC_SYNTHETIC = 0x1000;
		final int ACC_ENUM = 0x4000;
		String flags = "";
		if((accessFlag & ACC_PUBLIC) != 0)
		{
			flags += "public, ";
		}
		if((accessFlag & ACC_PRIVATE) != 0)
		{
			flags += "private, ";
		}
		if((accessFlag & ACC_PROTECTED) != 0)
		{
			flags += "protected, ";
		}
		if((accessFlag & ACC_STATIC) != 0)
		{
			flags += "static, ";
		}
		if((accessFlag & ACC_FINAL) != 0)
		{
			flags += "final, ";
		}
		if((accessFlag & ACC_VOLATILE) != 0)
		{
			flags += "volatile, ";
		}
		if((accessFlag & ACC_TRANSIENT) != 0)
		{
			flags += "transient, ";
		}
		if((accessFlag & ACC_SYNTHETIC) != 0)
		{
			flags += "synthetic, ";
		}
		if((accessFlag & ACC_ENUM) != 0)
		{
			flags += "enum, ";
		}
		return flags;
	}

	public static String resolveDataType(String rawType)
	{
		String dataType = null;
		switch(rawType)
		{
			case "C": 
				dataType = "char";
				break;
			case "D":
				dataType = "double";
				break;
			case "F":
				dataType = "float";
				break;
			case "I": 
				dataType = "int";
				break;
			case "S":
				dataType = "short";
				break;
			case "J":
				dataType = "long";
				break;
			case "Z":
				dataType = "boolean";
				break;
			case "[":
				dataType = "array";
				break;
			default:
				dataType = "string";
				break;
		}
		return dataType;
	}
}