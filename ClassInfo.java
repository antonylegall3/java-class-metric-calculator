import java.io.*;

/**
 * Parses the access flag, class name and super class 
 * information from a class file
 *
 * @author Antony LeGall
 */
public class ClassInfo
{
	public static String parseAccessFlag(int accessFlag)
	{
		//Performs an AND operation with each access flag to determine
		// flags are present
		final int ACC_PUBLIC = 0x0001;
		final int ACC_FINAL = 0x0010;
		final int ACC_SUPER = 0x0020;
		final int ACC_INTERFACE = 0x0200;
		final int ACC_ABSTRACT = 0x0400;
		final int ACC_SYNTHETIC = 0x1000;
		final int ACC_ANNOTATION = 0x2000;
		final int ACC_ENUM = 0x4000;

		String flags = "";
		if((accessFlag & ACC_PUBLIC) != 0)
		{
			flags += "public ";
		}
		if((accessFlag & ACC_FINAL) != 0)
		{
			flags += "final ";
		}
		if((accessFlag & ACC_INTERFACE) != 0)
		{
			flags += "interface ";
		}
		if((accessFlag & ACC_ABSTRACT) != 0)
		{
			flags += "abstract ";
		}
		if((accessFlag & ACC_SYNTHETIC) != 0)
		{
			flags += "synthetic ";
		}
		if((accessFlag & ACC_ANNOTATION) != 0)
		{
			flags += "annotation ";
		}
		if((accessFlag & ACC_ENUM) != 0)
		{
			flags += "enum ";
		}
		flags += "class";
		return flags;
	}

	public static String getClassName(int index, ConstantPool constantPool) throws 
						ClassFileParserException, IOException
	{
		String className;
		//Get the Constant Class entry containing the name index from the constant pool
		CPEntry cp = constantPool.getEntry(index);
		ConstantClass cc = (ConstantClass)cp;
		//Get the name index to find the name entry in the constant pool
		int nameIndex =cc.getNameIndex();
		//Get the Bytes(name) contained at the index found before
		CPEntry cp2 = constantPool.getEntry(nameIndex);
		ConstantUtf8 c8 = (ConstantUtf8) cp2;
		className = c8.getBytes();
		return className;
	}

	public static String getSuperName(int index, ConstantPool constantPool) throws 
						ClassFileParserException, IOException
	{
		String superName;
		//Get the Constant Class entry containing the name index from the constant pool
		CPEntry cp = constantPool.getEntry(index);
		ConstantClass cc = (ConstantClass)cp;
		//Get the name index to find the name entry in the constant pool
		int nameIndex =cc.getNameIndex();
		//Get the Bytes(name) contained at the index found before
		CPEntry cp2 = constantPool.getEntry(nameIndex);
		ConstantUtf8 c8 = (ConstantUtf8) cp2;
		superName = c8.getBytes();
		return superName;
	}

	public static String parseInterfaces(ConstantPool constantPool,
			DataInputStream dis) throws ClassFileParserException,
                                             IOException
	{
		//Simply parses and stores the interfaces for each class returning them as a string
		String interfaces = "";
		int numInterfaces = dis.readUnsignedShort();
		if(numInterfaces == 0)
		{
			interfaces = "Class does not implement any interfaces";
		}
		else
		{
			CPEntry cp;
			ConstantClass cc;
			int entry;
			for(int i = 0; i < numInterfaces; i++)
			{
				entry = dis.readUnsignedShort();
				cp = constantPool.getEntry(entry);
				cc = (ConstantClass)cp;
				interfaces += cc.getName();
			}
		}
		return interfaces;
	}
}