import java.io.*;
/**
 * Calculate the overall CBO for a group of classes
 *
 * @author Antony LeGall
 */
public class CalcCBO
{
	public static int calcCBO(ClassFile[] classArray) throws CodeParsingException, InvalidConstantPoolIndex
	{
		ClassFile cf;
		Method[] methods;
		Method method;
		Attribute attribute;
		byte[] bytes;
		CPEntry cpEntry;
		ConstantRef ref;
		ConstantClass cClass;
		ConstantPool constantPool;
		Instruction instruction;
		String className;
		int index, offset, classIndex, numLinks = 0, index1, index2;
		
		String[] strArray = createNameArray(classArray);
		int[][] indexArray = createIndexArray(classArray.length);

		for(int i = 0; i < classArray.length; i++)
		{
			cf = classArray[i];
			constantPool = cf.getConstantPool();
			methods = cf.getMethods();
			for(int j = 0; j < methods.length; j++)
			{
				method = methods[j];
				//Skip null methods such as <init>
				if(method != null)
				{
					//Get the Attributes from each method
					attribute = method.getAttribute();
					//Get the byte array for each methods code attribute
					bytes = attribute.getCodeArray();

					offset = 0;
					//Parse the byte array one instruction at a time
					while(offset < bytes.length)
					{
						index = 0;

						//Create instructions and choose the ones that pertain to method/field refs
						instruction = CalcLCom.createInstruction(bytes, offset);
						if(instruction.getValidRef() == true)
						{
							//Resolve the index into the constant pool
							index = (int)(((bytes[instruction.getOffset() + 1] & 0xFF) << 8) |
							(bytes[instruction.getOffset() + 2] & 0xFF));
							cpEntry = constantPool.getEntry(index);
							if(cpEntry instanceof ConstantMethodRef || (cpEntry instanceof ConstantFieldRef))
							{
								ref = (ConstantRef)cpEntry;
								//Get the class Index
								classIndex = ref.getClassIndex();
								//Get the entry at that index
								cpEntry = constantPool.getEntry(classIndex);
								cClass = (ConstantClass)cpEntry;
								//The name stored at that entry is the class its referencing
								className = cClass.getName();
								index1 = findEntry(strArray, className);
								index2 = findEntry(strArray, cf.getClassName());
								//If the indexes return -1 they are not a name of a class, if they are the same
								// its a class referencing itself which is not included
								if((index1 != -1) && (index2 != -2) && (index1!=index2))
								{
									numLinks = checkIndexArray(indexArray, numLinks, index1, index2);
								}
							}

						}
						offset += instruction.getSize();
					}
				}
			}
		}
		return numLinks;
	}

	public static String[] createNameArray(ClassFile[] classArray)
	{
		//Create an array of class names to be represented as integers
		ClassFile classObj;
		String[] nameArray = new String[classArray.length];
		for(int i = 0; i < classArray.length; i++)
		{
			classObj = classArray[i];
			nameArray[i] = classObj.getClassName();
		}
		return nameArray;
	}

	public static int[][] createIndexArray(int size)
	{
		//Create the adjacency list for each class and initialize it all to 0
		int[][] indexArray = new int[size][size];
		for(int x = 0; x < size; x++)
		{
			for(int y = 0; y < size; y++)
			{
				indexArray[x][y] = 0;
			}
		}
		return indexArray;
	}

	public static int checkIndexArray(int[][] indexArray, int numLinks, int index1, int index2)
	{
		//Update the adjacency list when a class references another
		if(indexArray[index1][index2] == 0)
		{
			indexArray[index1][index2] = 1;
			numLinks++;
		}
		return numLinks;
	}

	public static int findEntry(String[] strArray, String name)
	{
		int index = -1;
		//Find the index at which the provided string occurs in the array
		for(int i = 0; i < strArray.length; i++)
		{
			if(strArray[i].equals(name))
			{
				index = i;
			}
		}
		return index;
	}
}