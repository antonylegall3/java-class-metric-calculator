import java.io.*;

/**
 * Parse the methods belonging to the class and resolve their access
 * flags, names, dataTypes and Attributes
 *
 * @author Antony LeGall
 */
public class Method
{
	private String accessFlags;
	private String name;
	private String descriptor;
	private Attribute attribute;

	public Method(String inAccessFlags, String inName, String inDescriptor, Attribute inAttribute)
	{
		accessFlags = inAccessFlags;
		name = inName;
		descriptor = inDescriptor;
		attribute = inAttribute;
	}

	public String getAccessFlags()
	{
		return accessFlags;
	}

	public String getName()
	{
		return name;
	}

	public String getDescriptor()
	{
		return descriptor;
	}

	public Attribute getAttribute()
	{
		return attribute;
	}
	
	public static Method[] parseMethods(int count, ConstantPool constantPool,
			DataInputStream dis) throws ClassFileParserException,
                                             IOException
	{
		Method method;
		Method[] methods = new Method[count];
		Attribute attribute = null;
		String flags, methodName = null, descriptor = null, attributeName = null;
		CPEntry cp;
		ConstantUtf8 c8;
		int entry, attributeCount, attributeSize;
		for(int i = 0; i < count; i++)
		{
			//Get the Access Flags
			flags = parseAccessFlags(dis.readUnsignedShort());

			//Get the Method Name
			entry = dis.readUnsignedShort();
			cp = constantPool.getEntry(entry);
			c8 = (ConstantUtf8) cp;
			methodName = c8.getBytes();
			//Get the Descriptor
			entry = dis.readUnsignedShort();
			cp = constantPool.getEntry(entry);
			c8 = (ConstantUtf8) cp;
			descriptor = resolveDescriptor(c8.getBytes(), methodName);

			//Get the Count and create the array
			attributeCount = dis.readUnsignedShort();


			//Dont store static initializers
			if((methodName.equals("<init>")) || (methodName.equals("<clinit>"))
				|| (methodName.equals("equals")) || (methodName.equals("toString")))
			{
				for(int j = 0; j < attributeCount; j++)
				{
					//Read the name then the length then skip ahead length times
					dis.readUnsignedShort();
					attributeSize = dis.readInt();
					for(int k = 0; k < attributeSize; k++)
					{
						dis.readByte();
					}
				}
			}
			else
			{
				for(int j = 0; j < attributeCount; j++)
				{
					//Get the name of the attribute and parse it if its "code"
					entry = dis.readUnsignedShort();
					cp = constantPool.getEntry(entry);
					c8 = (ConstantUtf8) cp;
					attributeName = c8.getBytes();
					if(attributeName.equals("Code"))
					{
						attribute = Attribute.parseAttribute(dis, attributeName);
					}
					else
					{
						//Skip the attribute as its not Code
						attributeSize = dis.readInt();
						for(int k = 0; k < attributeSize; k++)
						{
							dis.readByte();
						}
					}
				}
				methods[i] = new Method(flags, methodName, descriptor, attribute);
			}
		}
		return methods;
	}

	public static String parseAccessFlags(int accessFlag)
	{
		//Performs an AND operation with each access flag to determine
		// flags are present
		final int ACC_PUBLIC = 0x0001;
		final int ACC_PRIVATE = 0x0002;
		final int ACC_PROTECTED = 0x0004;
		final int ACC_STATIC = 0x0008;
		final int ACC_FINAL = 0x0010;
		final int ACC_SYNCHRONIZED = 0x0020;
		final int ACC_BRIDGE = 0x0040;
		final int ACC_VARARGS = 0x0080;
		final int ACC_NATIVE = 0x0100;
		final int ACC_ABSTRACT = 0x0400;
		final int ACC_STRICT = 0x0800;
		final int ACC_SYNTHETIC = 0x1000;
		
		String flags = "";
		if((accessFlag & ACC_PUBLIC) != 0)
		{
			flags += "Public, ";
		}
		if((accessFlag & ACC_PRIVATE) != 0)
		{
			flags += "Private, ";
		}
		if((accessFlag & ACC_PROTECTED) != 0)
		{
			flags += "Protected, ";
		}
		if((accessFlag & ACC_STATIC) != 0)
		{
			flags += "Static, ";
		}
		if((accessFlag & ACC_FINAL) != 0)
		{
			flags += "Final, ";
		}
		if((accessFlag & ACC_SYNCHRONIZED) != 0)
		{
			flags += "Synchronized, ";
		}
		if((accessFlag & ACC_BRIDGE) != 0)
		{
			flags += "Bridge, ";
		}
		if((accessFlag & ACC_VARARGS) != 0)
		{
			flags += "Varargs, ";
		}
		if((accessFlag & ACC_NATIVE) != 0)
		{
			flags += "Native, ";
		}
		if((accessFlag & ACC_ABSTRACT) != 0)
		{
			flags += "Abstract, ";
		}
		if((accessFlag & ACC_STRICT) != 0)
		{
			flags += "Strict, ";
		}
		if((accessFlag & ACC_SYNTHETIC) != 0)
		{
			flags += "Synthetic, ";
		}
		return flags;
	}

	public static String resolveDescriptor(String rawDescriptor, String methodName)
	{
		//Resolves the decriptor for a given method
		String descriptor = "";
		char type;
		//Get the return first located at the end
		descriptor += getDescriptor(rawDescriptor.charAt(rawDescriptor.length() - 1));
		descriptor += methodName;
		//Iterate to -1 as the last char has already been resolved
		for(int i = 0; i < rawDescriptor.length() - 1; i++)
		{
			type = rawDescriptor.charAt(i);
			if(type == 'L')
			{
				while(type != ';')
				{
					i++;
					type= rawDescriptor.charAt(i);
					descriptor += type;
				}
			}
			else if(type == '[')
			{
				//Get the next type first
				descriptor += getDescriptor(rawDescriptor.charAt(i++));
				descriptor += "[]";
				//Increase i by 1 as we already parsed the next item
				i++;
			}
			else
			{
				descriptor += getDescriptor(type);
			}
		}
		return descriptor;
	}

	public static String getDescriptor(char type)
	{
		String dataType = null;
		switch(type)
		{
			case 'B':
				dataType = "byte ";
				break;
			case 'C': 
				dataType = "char ";
				break;
			case 'D':
				dataType = "double ";
				break;
			case 'F':
				dataType = "float ";
				break;
			case 'I': 
				dataType = "int ";
				break;
			case 'S':
				dataType = "short ";
				break;
			case 'J':
				dataType = "long ";
				break;
			case 'Z':
				dataType = "boolean ";
				break;
			case 'V':
				dataType = "void ";
				break;
			case '(':
				dataType = "(";
				break;
			case ')':
				dataType = ")";
				break;
		}
		return dataType;
	}
}