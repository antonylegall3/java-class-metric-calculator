README for a application to calculate standard software metrics on provided java class files. The application calculates the LCOM4 and the CBO of a given class file which can be useful in determining code quality.

Repository File Structure:
	All src files present in main directory

This application was created using:
	- Java 1.8.0.111
	
Compilation:
	- Navigate to the main directory
	
		$ javac *.java
		
Running:
	- Navigate to the main directory
	
		$ java CalcMetrics example.class example2.class example3.class
	
Contact Details:
	- Email: aantlegall@gmail.com