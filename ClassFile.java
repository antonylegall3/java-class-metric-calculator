import java.io.*;

/**
 * Parses and stores a Java .class file.
 *
 * @author David Cooper
 * Modified by Antony LeGall
 */
public class ClassFile
{
    private String filename;
    private long magic;
    private int minorVersion;
    private int majorVersion;
    private ConstantPool constantPool;
    private String accessFlags;
    private String className;
    private String superName;
    private String interfaces;
    private Field[] fields;
    private Method[] methods;
    private Attribute[] attributes;

    public ClassFile(String filename) throws ClassFileParserException,
                                             IOException
    {
        DataInputStream dis =
            new DataInputStream(new FileInputStream(filename));

        this.filename = filename;
        magic = (long)dis.readUnsignedShort() << 16 | dis.readUnsignedShort();
        //Header Error checking
        long realMagic = 3405691582L;
        if(magic != realMagic)
        {
            throw new InvalidHeaderException("Invalid Format(Magic Number) Identifier, File does not contain a java class");
        }
        minorVersion = dis.readUnsignedShort();
        majorVersion = dis.readUnsignedShort();
        if(majorVersion != 51)
        {
          throw new InvalidHeaderException("Class File is of unsupported type. Please provide a class file of version 51.0");
         }
        constantPool = new ConstantPool(dis);

        // Parse the rest of the class file
        accessFlags = ClassInfo.parseAccessFlag(dis.readUnsignedShort());
        className = ClassInfo.getClassName(dis.readUnsignedShort(), constantPool);
        superName = ClassInfo.getSuperName(dis.readUnsignedShort(), constantPool);
        interfaces = ClassInfo.parseInterfaces(constantPool, dis);

        //Parse the fields and methods
        fields = Field.parseFields(dis.readUnsignedShort(), constantPool, dis);
        methods = Method.parseMethods(dis.readUnsignedShort(), constantPool, dis);

        //Skip the Class Attributes unless they are "code"
        int count = dis.readUnsignedShort();
        for(int i = 0; i < count; i++)
        {
            CPEntry cp;
            ConstantUtf8 c8;
            int entry, length;
            String name;
            entry = dis.readUnsignedShort();
            cp = constantPool.getEntry(entry);
            c8 = (ConstantUtf8) cp;
            name = c8.getBytes();
            if(name.equals("Code"))
            {
                attributes[i] = Attribute.parseAttribute(dis, name);
            }
            else
            {
                length = dis.readInt();
                for(int j = 0; j < length; j++)
                {
                    dis.readByte();
                }
            }
        }
    }

    public Method[] getMethods() { return methods;}
    public ConstantPool getConstantPool() { return constantPool; }
    public Field[] getFields() { return fields; }
    public String getClassName() { return className; }

    /** Returns the contents of the class file as a formatted String. */
    public String toString()
    {
        return String.format("%s %s:",accessFlags, className);
    }
}

/**
 * Thrown when the magic number is not cafebabe or if the version isnt 51.0
 */
class InvalidHeaderException extends ClassFileParserException
{
    public InvalidHeaderException(String msg) { super(msg); }
}